const form = document.querySelector("#new-task-form");
const input = document.querySelector("#new-task-input");
const list_element = document.querySelector("#tasks");
const main_element = document.querySelector("main");
const task_footer = document.querySelector(".task-footer");
const no_of_task = document.querySelector(".number-of-task");
const all_task = document.querySelector(".all-task");
const active_task = document.querySelector(".active-task");
const completed_task = document.querySelector(".completed-task");
// Load tasks from localStorage on window load
let tasklist = 0;
const savedTasks = JSON.parse(localStorage.getItem("tasks")) || [];
savedTasks.forEach((taskText) => {
  createTask(taskText);
});

form.addEventListener("submit", (e) => {
  e.preventDefault();
  // Trim whitespace from the input
  const task = input.value.trim();
  if (!task) {
    alert("Please add a task");
    return;
  }

  createTask(task);
  saveintoLocalstorage(task);

  // Clear input field after adding task
  input.value = "";
});

function createTask(taskText) {
  const task_element = document.createElement("div");
  task_element.classList.add("task", "flex");
  const task_content = document.createElement("div");
  task_content.classList.add("content");
  task_element.appendChild(task_content);

  const checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.classList.add("check-box");
  checkbox.name = "checkbox";
  task_content.appendChild(checkbox);

  const task_label = document.createElement("label");
  task_label.innerText = taskText;
  task_label.for = "checkbox";
  task_content.appendChild(task_label);

  const task_input = document.createElement("input");
  task_input.classList.add("text");
  task_input.type = "text";
  task_input.value = taskText;
  task_input.setAttribute("readonly", "readonly");
  task_content.appendChild(task_input);

  no_of_task.innerText = tasklist;

  const task_actions = document.createElement("div");
  task_actions.classList.add("actions");
  const task_edit = document.createElement("button");
  task_edit.classList.add("edit");
  task_edit.innerHTML = "Edit";
  const task_delete = document.createElement("button");
  task_delete.classList.add("delete");
  task_delete.innerHTML = "Delete";
  task_actions.appendChild(task_edit);
  task_actions.appendChild(task_delete);
  task_element.appendChild(task_actions);
  list_element.appendChild(task_element);
  tasklist++;

  //count number of tasks
  no_of_task.innerText = "Tasks: " + tasklist;

  //edit button functionalitu
  task_edit.addEventListener("click", () => {
    if (task_edit.innerText.toLowerCase() === "edit") {
      task_input.removeAttribute("readonly");
      task_input.focus();
      task_edit.innerText = "Save";
    } else {
      task_input.setAttribute("readonly", "readonly");
      task_edit.innerText = "Edit";
      updateLocalstorage(taskText, task_input.value);
      taskText = task_input.value;
    }
  });

  //delete button functinality
  task_delete.addEventListener("click", () => {
    list_element.removeChild(task_element);
    deletefromLocalstorage(taskText);
    tasklist--;
    no_of_task.innerText = "Tasks: " + tasklist;
  });

  all_task.addEventListener("click", () => {
    list_element.childNodes.forEach((each) => {
      each.classList.remove("none");
      each.classList.add("flex");
    });
  });

  active_task.addEventListener("click", () => {
    list_element.childNodes.forEach((each) => {
      each.classList.add("flex");
      if (each.childNodes[0].childNodes[0].checked) {
        each.classList.remove("flex");
        each.classList.add("none");
      } else {
        each.classList.add("flex");
        each.classList.remove("none");
      }
    });
  });

  completed_task.addEventListener("click", () => {
    list_element.childNodes.forEach((each) => {
      each.classList.add("flex");
      if (each.childNodes[0].childNodes[0].checked) {
        each.classList.remove("none");
        each.classList.add("flex");
      } else {
        each.classList.add("none");
        each.classList.remove("flex");
      }
    });
  });
}

function saveintoLocalstorage(task) {
  const savedTasks = JSON.parse(localStorage.getItem("tasks")) || [];
  savedTasks.push(task);
  localStorage.setItem("tasks", JSON.stringify(savedTasks));
}

function updateLocalstorage(oldTask, newTask) {
  const savedTasks = JSON.parse(localStorage.getItem("tasks")) || [];
  const index = savedTasks.indexOf(oldTask);
  if (index !== -1) {
    savedTasks[index] = newTask;
    localStorage.setItem("tasks", JSON.stringify(savedTasks));
  }
}

function deletefromLocalstorage(task) {
  const savedTasks = JSON.parse(localStorage.getItem("tasks")) || [];
  const index = savedTasks.indexOf(task);
  if (index !== -1) {
    savedTasks.splice(index, 1);
    localStorage.setItem("tasks", JSON.stringify(savedTasks));
  }
}
